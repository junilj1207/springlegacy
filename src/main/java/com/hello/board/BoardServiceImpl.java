package com.hello.board;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.html2pdf.resolver.font.DefaultFontProvider;
import com.itextpdf.io.font.FontProgram;
import com.itextpdf.io.font.FontProgramFactory;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.IBlockElement;
import com.itextpdf.layout.element.IElement;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.font.FontProvider;


@Service
public class BoardServiceImpl implements BoardService{
	
	@Autowired
	BoardDao boardDao;

	//게시글 입력
	@Override
	public void create(Map<String, Object> map) {
		 String title  = map.get("title").toString(); 
		 String content = map.get("content").toString(); 
		 String writer = map.get("writer").toString(); 
		
	     title = title.replace("<", "&lt;");
	     title = title.replace("<", "&gt;");
	     writer = writer.replace("<", "&lt;");
	     writer = writer.replace("<", "&gt;");
         title = title.replace("  ",    "&nbsp;&nbsp;");
         writer = writer.replace("  ",    "&nbsp;&nbsp;");
         content = content.replace("\n", "<br>");
         
         Map<String,Object> newMap=new HashMap<String,Object>(); 
         newMap.put("title", title);
         newMap.put("content", content);
         newMap.put("writer", writer);
         this.boardDao.insert(newMap);     
	}

	//게시글 조회
	@Override
	public List<BoardDto> listAll(Map<String, Object> map) {
		
		return this.boardDao.selectAll(map); 
	}

	//게시글 하나조회
	@Override
	public Map<String,Object> detailView(int bno) {
		
		return this.boardDao.viewDetail(bno); 
	} 
	
	
	
	//itext5 버전
	//pdf 파일 변환
//	@Override
//	public void createPdfbasic(Map<String, String> map) {
//	
//		System.out.println("안녕하세요!");
//        String htmlFilePath = "html/htmlfile.html"; // HTML 파일의 리소스 경로
//
//        String outputPath = "c:\\data\\pdf\\example.pdf"; // PDF 파일을 저장할 경로 및 파일명
//
//        File directory = new File("c:\\data\\pdf");
//        if (!directory.exists()) {
//            directory.mkdirs();
//        }
//        
//        
//        
//   
//        Document document = new Document(PageSize.A4.rotate(),10,10,10,10);
//
//        try {
//        	
//
//        	
//        	PdfWriter.getInstance(document,new FileOutputStream(outputPath)); 
//        	
//            document.open();
//
//            Paragraph paragraph = new Paragraph("안녕하세요, iText를 사용한 PDF 생성 예시입니다!");
//            document.add(paragraph);
//
//            // 문서 닫기
//            document.close();
//        }catch(Exception e) {
//        	e.printStackTrace();
//        }
//        
//        
//	}

	//itext5 버전
//	@Override
//	public void createPdfupgrade(Map<String, String> map) {
//		System.out.println("안녕하세요!");
//
//        String outputPath = "c:\\data\\pdf\\example.pdf"; // PDF 파일을 저장할 경로 및 파일명
//
//        File directory = new File("c:\\data\\pdf");
//        if (!directory.exists()) {
//            directory.mkdirs();
//        }
//        
//        
//        String[] data = {
//                "데이터 1: 이것은 첫 번째 데이터입니다.",
//                "데이터 2: 이것은 두 번째 데이터입니다.",
//                "데이터 3: 이것은 세 번째 데이터입니다.",
//                // ... 나머지 데이터도 추가하세요
//            };
//   
//        Document document = new Document(PageSize.A4.rotate(),10,10,10,10);
//
//        try {
//        	
//        	PdfWriter.getInstance(document,new FileOutputStream(outputPath)); 
//        	
//            document.open();
//
//            for (String item : data) {
//                // 새로운 페이지 생성
//                document.newPage();
//                // 데이터 추가
//                Paragraph paragraph = new Paragraph(item);
//                document.add(paragraph);
//            }
//
//            // 문서 닫기
//            document.close();
//        }catch(Exception e) {
//        	e.printStackTrace();
//        }		
//	}
//	
	
	@Override
	public void createPdfbasic(Map<String, String> map) {
		

	    System.out.println("안녕하세요!");
	    String outputPath = "c:\\data\\pdf\\example.pdf"; // PDF 파일을 저장할 경로 및 파일명

	    File directory = new File("c:\\data\\pdf");
	    if (!directory.exists()) {
	        directory.mkdirs();
	    }

	    try {
	    
	    	//--------------------폰트 설정----------------------------------
	    	
		    String FONT = "../../resources/font/GyeonggiTitleL.ttf";
		    ConverterProperties properties = new ConverterProperties();
		    FontProvider fontProvider = new DefaultFontProvider(false, false, false);
		    FontProgram fontProgram = FontProgramFactory.createFont(FONT);
		    fontProvider.addFont(fontProgram);
		    properties.setFontProvider(fontProvider);
		    //--------------------폰트 설정----------------------------------	
		    
		    
		    //--------------------html 설정----------------------------------	
		    //표지
		    StringBuffer appenHtmlCover = new StringBuffer();
		    appenHtmlCover.append("<!DOCTYPE html>"); 
		    appenHtmlCover.append("<html lang=\"ko\">");
		    appenHtmlCover.append("<head>");
		    appenHtmlCover.append("<meta charset=\"UTF-8\">");
		    appenHtmlCover.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
		    appenHtmlCover.append(htmlCss());
		    appenHtmlCover.append("<title>유지보수점검일지</title>");
		    appenHtmlCover.append("</head>");
		    appenHtmlCover.append("<body>");
		    appenHtmlCover.append(htmlTable("서강대학교","LINC",null,"","정준일","9"));
		    appenHtmlCover.append("</body>");
		    
		    //내용
		    StringBuffer appenHtmlInfo = new StringBuffer();
		    appenHtmlInfo.append("<!DOCTYPE html>"); 
		    appenHtmlInfo.append("<html lang=\"ko\">");
		    appenHtmlInfo.append("<head>");
		    appenHtmlInfo.append("<meta charset=\"UTF-8\">");
		    appenHtmlInfo.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
		    appenHtmlInfo.append(htmlCss());
		    appenHtmlInfo.append("<title>유지보수점검일지</title>");
		    appenHtmlInfo.append("</head>");
		    appenHtmlInfo.append("<body>");
		    appenHtmlInfo.append(htmlSrLog());		    		
		    appenHtmlInfo.append("</body>");

		    //--------------------html 설정----------------------------------	
			
		    
		    //해당 배열만큼 pdf 페이지가 그려진다.
			String[]  htmlFiles = {appenHtmlCover.toString(),appenHtmlInfo.toString()};
			
		    PdfDocument pdf = new PdfDocument(new PdfWriter(outputPath));
		    Document document = new Document(pdf,PageSize.A4.rotate());
		    document.setMargins(50, 50, 50, 50);
		    
		       for (int i=0; i<htmlFiles.length; i++) {
		    	   
	                String htmlContent = htmlFiles[i];
	                List<IElement> elements = HtmlConverter.convertToElements(htmlContent, properties);

		                for (IElement element : elements) {
		                    document.add((IBlockElement) element);

			                //마지막 페이지 빈 페이지 안생기게 하기
			                if(i==htmlFiles.length-1)   break;
		                	
			                document.add(new com.itextpdf.layout.element.AreaBreak());

		                }
	            }
		    document.close();
        
	   } catch (Exception e) {
	        e.printStackTrace();
	    }
		
	}
	
	protected static String htmlCss() {
		
		StringBuffer cssBuffer = new StringBuffer();
		cssBuffer.append("<style>");
		cssBuffer.append("@page {size: A4 landscape;");
		cssBuffer.append("margin: 0;");
		cssBuffer.append("}");
		
		cssBuffer.append("body {");
		cssBuffer.append("display: flex;");
		cssBuffer.append("justify-content: center;");
		cssBuffer.append("align-items: center;");
		cssBuffer.append("min-height: 100vh;");
		cssBuffer.append("margin: 0;");
		cssBuffer.append("padding: 0 20%;");
		cssBuffer.append("}");
		
		cssBuffer.append("table {");
		cssBuffer.append("margin : auto;");
		cssBuffer.append("width : 100%;");
		cssBuffer.append("border-collapse: collapse;");
		cssBuffer.append("}");
		
		cssBuffer.append("th, td {");
		cssBuffer.append("border: 1px solid #444444;");
		cssBuffer.append("padding: 10px;");
		cssBuffer.append("}");
		
		cssBuffer.append(".titleText {");
		cssBuffer.append("text-align: center;");
		cssBuffer.append("font-size: 30px;");
		cssBuffer.append("border: none;");
		cssBuffer.append("}");
		
		cssBuffer.append(".subText {");
		cssBuffer.append("text-align: center;");
		cssBuffer.append("font-size: 13px;");
		cssBuffer.append("}");
		
		cssBuffer.append(".subtitleText {");
		cssBuffer.append(" text-align: left;");
		cssBuffer.append("  font-size: 18px;");
		cssBuffer.append("    border: none;");
		cssBuffer.append("}");
		
		cssBuffer.append(".textOver {");
		cssBuffer.append(" vertical-align: top;");
		cssBuffer.append(" height: 70px;");
		cssBuffer.append(" overflow: hidden;");
		cssBuffer.append(" font-size: 11px;");
		cssBuffer.append("}");

		cssBuffer.append("</style>");

		String cssString = cssBuffer.toString();
		return cssString;
	}
	
	protected static String htmlTable(String schNm ,String prjNm, String[] count,String allCount, String manageNm, String month) {
		
		StringBuffer tableBuffer = new StringBuffer();
		
		tableBuffer.append("<table>");
		tableBuffer.append("<tr>");
		tableBuffer.append("<td class=\"titleText\" height=\"100\" colspan=\"5\">"+schNm+"("+prjNm+")"+" 시스템 유지보수 점검일지 - "+month+"월</td>");	
		tableBuffer.append("</tr>");
		tableBuffer.append("<tr height=\"20%\">");
		tableBuffer.append("<th>대상</th>");
		tableBuffer.append("<td class=\"subText\" colspan=\"4\">"+schNm+"("+prjNm+")"+" 시스템</td>");
		tableBuffer.append("</tr>");
		tableBuffer.append("<tr>");
		tableBuffer.append("<th>목적</th>");
		tableBuffer.append("<td class=\"subText\" colspan=\"4\"><input type=\"checkbox\">설치, <input type=\"checkbox\" checked>정기점검, <input type=\"checkbox\" checked>장애처리, <input type=\"checkbox\" checked>업그레이드");
		tableBuffer.append("<br>기타(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>");
		tableBuffer.append("</tr>");
		tableBuffer.append("<tr>");
		tableBuffer.append("<th rowspan=\"5\">작업 내용<br> 및<br> 조치 사항</th>");
		tableBuffer.append("<td class=\"subText\">사용자 오류</td>");
		tableBuffer.append("<td class=\"subText\">0</td>");
		tableBuffer.append("<td class=\"subText\">데이터 오류</td>");
		tableBuffer.append("<td class=\"subText\">0</td>");
		tableBuffer.append("<tr>");
		tableBuffer.append("<td class=\"subText\">기능 오류</td>");
		tableBuffer.append("<td class=\"subText\">0</td>");
		tableBuffer.append("<td class=\"subText\">표기 오류</td>");
		tableBuffer.append("<td class=\"subText\">0</td>");
		tableBuffer.append("</tr>");
		tableBuffer.append("<tr>");
		tableBuffer.append("<td class=\"subText\">변&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;경</td>");
		tableBuffer.append("<td class=\"subText\">0</td>");
		tableBuffer.append("<td class=\"subText\">수&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;정</td>");
		tableBuffer.append("<td class=\"subText\">0</td>");
		tableBuffer.append("</tr>");
		tableBuffer.append("<tr>");
		tableBuffer.append("<td class=\"subText\">추&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;가</td>");
		tableBuffer.append("<td class=\"subText\">0</td>");
		tableBuffer.append("<td class=\"subText\">문&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;의</td>");
		tableBuffer.append("<td class=\"subText\">0</td>");
		tableBuffer.append("</tr>");
		tableBuffer.append("<tr>");
		tableBuffer.append("<td class=\"subText\">확&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;인</td>");
		tableBuffer.append("<td class=\"subText\">0</td>");
		tableBuffer.append("<td class=\"subText\">요&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;청</td>");
		tableBuffer.append("<td class=\"subText\">0</td>");
		tableBuffer.append("</tr>");
		tableBuffer.append("<tr>");
		tableBuffer.append("<th>특이사항</th>");
		tableBuffer.append("<td class=\"subText\" colspan=\"4\">특이사항 없습니다.</td>");
		tableBuffer.append("</tr>");
		tableBuffer.append("<tr>");
		tableBuffer.append("<th>유지보수업체명</th>");
		tableBuffer.append("<td class=\"subText\" colspan=\"4\">(주)다인리더스</td>");
		tableBuffer.append("</tr>");
		tableBuffer.append("<tr>");
		tableBuffer.append("<th rowspan=\"2\">작업자 확인</th>");
		tableBuffer.append("<td class=\"subText\" colspan=\"2\">작업자</td>");
		tableBuffer.append("<td class=\"subText\" colspan=\"2\">확인자</td>");
		tableBuffer.append("</tr>");
		tableBuffer.append("<tr>");
		tableBuffer.append("<td class=\"subText\" colspan=\"2\">");
		tableBuffer.append(manageNm+"</td>");
		tableBuffer.append("<td class=\"subText\" colspan=\"2\">박영민</td>");
		tableBuffer.append("</tr>");
		tableBuffer.append("</table>");

		String tableString = tableBuffer.toString();
		
		return tableString;
	}
	
	protected String htmlSrLog() {
		StringBuffer htmlBuffer = new StringBuffer();

		htmlBuffer.append("<table>");

		// 첫 번째 행
		htmlBuffer.append("<tr>");
		htmlBuffer.append("<td class=\"subText\">접수번호</td>");
		htmlBuffer.append("<td class=\"subText\">1234</td>");
		htmlBuffer.append("<td class=\"subText\">고객사명</td>");
		htmlBuffer.append("<td class=\"subText\">고객 담당자</td>");
		htmlBuffer.append("<td class=\"subText\">유수정</td>");
		htmlBuffer.append("<td class=\"subText\">접수일시</td>");
		htmlBuffer.append("<td class=\"subText\">20222323</td>");
		htmlBuffer.append("<td class=\"subText\">완료일자</td>");
		htmlBuffer.append("<td class=\"subText\">20230203</td>");
		htmlBuffer.append("</tr>");

		// 두 번째 행
		htmlBuffer.append("<tr>");
		htmlBuffer.append("<td class=\"subText\">처리담당자</td>");
		htmlBuffer.append("<td class=\"subText\">정준일</td>");
		htmlBuffer.append("<td class=\"subText\">요청구분</td>");
		htmlBuffer.append("<td class=\"subText\">요청</td>");
		htmlBuffer.append("<td class=\"subText\">메뉴경로</td>");
		htmlBuffer.append("<td class=\"subText\"colspan=\"5\">관리리자>페이지</td>");
		htmlBuffer.append("</tr>");

		// 세 번째 행
		htmlBuffer.append("<tr>");
		htmlBuffer.append("<td class=\"subtitleText\" colspan=\"6\">[요청내용]</td>");
		htmlBuffer.append("</tr>");

		// 네 번째 행
		htmlBuffer.append("<tr>");
		htmlBuffer.append("<td colspan=\"9\" >");
		htmlBuffer.append("<div class=\"textOver\">");
		htmlBuffer.append("11학생조회가 안되서 최초 문의드렸으나, 초기화 후 학생을 검색하면, 정상적으로 검색이 됨을 확인하였습니다.1. 학생 조회에 어떤 디폴트가 걸려있는지? 아마 재학인거 같은데 학생 조회가 원활하지 않아 선택으로 설정 부탁드립니다. 2. 휴학인 김온유 202127864(수정) 이 학생은 학생페이지에서 정상적으로 비교과 활동증명서를 출력할 수 있는지 문의드립니다.+ 학생에게 확인해보니 비교과활동증명서를 출력할 수 없어서 연락드렸습니다. 출력이 불가능합니다. 교내 비교과프로그램 활동 증명서 출력 창구가 아주허브 이기 때문에, 학생들 증명서 출력에 어려움이 없어야합니다. 감사합니다.");
		htmlBuffer.append("</div>");
		htmlBuffer.append("</td>");
		htmlBuffer.append("</tr>");

		htmlBuffer.append("<tr>");
		htmlBuffer.append("<td class=\"subtitleText\" colspan=\"6\">[답변 내용]</td>");
		htmlBuffer.append("</tr>");

		htmlBuffer.append("<tr>");
		htmlBuffer.append("<td colspan=\"9\" >");
		htmlBuffer.append("<div class=\"textOver\">");
		htmlBuffer.append("22안녕하세요 선생님, 접수해주신 해당 건 수정 완료하였습니다. 확인 부탁드립니다. 감사합니다.");
		htmlBuffer.append("</div>");
		htmlBuffer.append("</td>");
		htmlBuffer.append("</tr>");

		htmlBuffer.append("<tr>");
		htmlBuffer.append("<td class=\"subtitleText\" colspan=\"6\">[시스템 적용 범위]</td>");
		htmlBuffer.append("</tr>");

		htmlBuffer.append("<tr>");
		htmlBuffer.append("<td colspan=\"9\" >");
		htmlBuffer.append("<div class=\"textOver\">");
		htmlBuffer.append("44인터페이스 확인 작업");
		htmlBuffer.append("</div>");
		htmlBuffer.append("</td>");	
		htmlBuffer.append("</tr>");

		htmlBuffer.append("<tr>");
		htmlBuffer.append("<td class=\"subtitleText\" colspan=\"6\">[요청 확인 결과]</td>");
		htmlBuffer.append("</tr>");

		htmlBuffer.append("<tr>");
		htmlBuffer.append("<td colspan=\"9\" >");
		htmlBuffer.append("<div class=\"textOver\">");
		htmlBuffer.append("33인터페이스 확인 작업");
		htmlBuffer.append("</div>");
		
		htmlBuffer.append("</tr>");

		htmlBuffer.append("</table>");

		// StringBuffer를 문자열로 변환
		String fullHTML = htmlBuffer.toString();
		return fullHTML;
	}
	
	@Override
	public void createPdfupgrade(Map<String, String> map) {
	
	}

}
