package com.hello.util;

import java.io.*;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.FileCopyUtils;

public class FileUtil {

	
	
	public static List<File> copyFileToTemp(String _fileName) throws Exception {
		
			List<File> files = new ArrayList<File>();
			String streamNm = "";


			//String filePath = "C:\\data\\excel"; 
			String filePath = File.separator+"data"+File.separator+"excel"; 
			
			String fileOrgname=_fileName; 
			String fileExt = "xlsx";
			String tempPath = filePath+File.separator + "temp"; 
			
			streamNm = tempPath + File.separator + fileOrgname;
			
			File tempDir = new File(tempPath);
			if(!tempDir.exists()) tempDir.mkdirs();
			FileInputStream inputStream = new FileInputStream(filePath + File.separator +File.separator+fileOrgname);
			FileOutputStream outputStream = new FileOutputStream(streamNm);
			FileChannel fcin =  inputStream.getChannel();
			FileChannel fcout = outputStream.getChannel();
			
			long size = fcin.size();
			fcin.transferTo(0, size, fcout);
			
			fcout.close();
			fcin.close();
			outputStream.close();
			inputStream.close();
			files.add(new File(streamNm));
		
		
		return files; 
	}
	
	public static void delete(String deleteRealPath) {
		// 경로
		File fileData = new File(deleteRealPath);

		// 파일 존재 여부
		if (fileData.exists()) {
			fileData.delete();
		}
	}
	
	
	public static void createZipFile(String fullPath, List<File> fileList) throws Exception {
		FileUtil.delete(fullPath);

		FileOutputStream fout = new FileOutputStream(fullPath);
		ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(fout));

		for (File file : fileList) {

			zos.putNextEntry(new ZipEntry(file.getName()));

			// Get the file
			FileInputStream fis = new FileInputStream(file);
			BufferedInputStream fif = new BufferedInputStream(fis);

			// Write the contents of the file
			int data = 0;
			while ((data = fif.read()) != -1) {
				zos.write(data);
			}
			fif.close();

			zos.closeEntry();
		}

		zos.close();
		fout.close();
		
	}
	
	
	//파일 다운로드
	public static String download(	
			HttpServletRequest request, 
			HttpServletResponse response, 
			String originalFileName, 
			String uploadedRealPath, 
			String saveFileName
			) throws Exception {
	
		response.setContentType(request.getSession().getServletContext().getMimeType(uploadedRealPath));
		String fileName = setDisposition(originalFileName, request, response);
		uploadedRealPath = uploadedRealPath + File.separator + saveFileName;

		
		File file = new File(uploadedRealPath);
		FileInputStream fileIn = null;
		ServletOutputStream out = response.getOutputStream();

		try {
			fileIn = new FileInputStream(file);

			FileCopyUtils.copy(fileIn, out);
		} catch (Exception e) {
			e.printStackTrace();
		}  finally {
			if (fileIn != null) {
				fileIn.close();
			}
			out.flush();
			out.close();
		}
	
		
		return fileName;

	}
	
	
	
	private static String getBrowser(HttpServletRequest request) {
		String header = request.getHeader("User-Agent");
		if (header.indexOf("MSIE") > -1) {
			return "MSIE";
		} else if (header.indexOf("Trident") > -1) { // IE11 문자열 깨짐 방지
			return "Trident";
		} else if (header.indexOf("Chrome") > -1) {
			return "Chrome";
		} else if (header.indexOf("Opera") > -1) {
			return "Opera";
		}
		return "Firefox";
	}
	
	
	public static String setDisposition(String filename, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String browser = getBrowser(request);

		String dispositionPrefix = "attachment; filename=";
		String encodedFilename = null;
		
		filename = filename.replaceAll(",", "");

		if (browser.equals("MSIE")) {
			encodedFilename = URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20");
		} else if (browser.equals("Trident")) { // IE11 문자열 깨짐 방지
			encodedFilename = URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20");
		} else if (browser.equals("Firefox")) {
			encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
		} else if (browser.equals("Opera")) {
			encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
		} else if (browser.equals("Chrome")) {
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < filename.length(); i++) {
				char c = filename.charAt(i);
				if (c > '~') {
					sb.append(URLEncoder.encode("" + c, "UTF-8"));
				} else {
					sb.append(c);
				}
			}
			encodedFilename = sb.toString();
		} else {
			throw new IOException("Not supported browser");
		}

		response.setHeader("Content-Disposition", dispositionPrefix + encodedFilename);

		if ("Opera".equals(browser)) {
			response.setContentType("application/octet-stream;charset=UTF-8");
		}

		return encodedFilename;
	}
	
	
}
