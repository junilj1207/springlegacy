package com.hello.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel {
	
	
	
	
	
	public static void createDownloadExcelSheet(
			
			HttpServletResponse response, 
			String fileName,
			List<List<String>> fieldAll, 
			List<List<Map<String, Object>>> dataAll,
			List<Map<String, String>> labelMapperAll, 
			List<String> sheetName
			
			
			) throws IOException {
		response.setContentType("application/vnd.ms-excel");

		// creates mock data
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", fileName);
		response.setHeader(headerKey, headerValue);
		// createExcelFile( response.getOutputStream(), field, data,
		// labelMapper);
//		createExcelFileSheet("",response.getOutputStream(), fieldAll, dataAll, labelMapperAll,
//				sheetName);
	}
	
	// 엑셀 다운로드 시트 추가하기_동아리관리 엑셀다운에서 사용
	public static boolean createExcelFileSheet(
			String outputPath,
			List<List<String>> fieldAll,
			List<List<Map<String, Object>>> dataAll, 
			List<Map<String, String>> labelMapperAll,
			List<String> sheetName 
			
			) throws IOException {
		
		boolean isSuccess = false; 

		SXSSFWorkbook xlsxWb = new SXSSFWorkbook(); // Excel 2007 이후버젼
		SXSSFRow row = null;
		SXSSFCell cell = null;
		FileOutputStream fileOut = null;
		// WorkBook 생성

		// 쎌 스타일
		CellStyle headerStyle = xlsxWb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		headerStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		headerStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);

		CellStyle bodyStyle = xlsxWb.createCellStyle();
		bodyStyle.setAlignment(CellStyle.ALIGN_CENTER);
		bodyStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);


		try {
			// 헤더
			for (int a = 0; a < fieldAll.size(); a++) { //여기 for문은 무조건 한번만 돈다(a는 0이다.)

				// WorkSheet 생성
				SXSSFSheet sheet =xlsxWb.createSheet(sheetName.get(a));
			
				// field 리스트에서 field 하나를 가지고 온다
				List<String> field = fieldAll.get(a);
				
				// labelMapper 리스트에서 labelMapper 하나를 가지고 온다
				Map<String, String> labelMapper = labelMapperAll.get(a);
				
				// data 리스트에서 data 하나를 가지고 온다
				List<Map<String, Object>> data = dataAll.get(a);

				int numField = field.size(); //필드 갯수
				row = sheet.createRow(0);
				
				//필드(헤더 세팅)---------------------------
				for (int i = 0; i < numField; i++) {
					String l = null;
					if (labelMapper != null) {
						l = labelMapper.get(field.get(i));
					} else {
						l = field.get(i);
					}

					cell = row.createCell(i);
					cell.setCellValue(l);
					cell.setCellStyle(headerStyle);
				}
				//필드(헤더 세팅)---------------------------

				
				//실데이터 입력---------------------------
				int numData = data.size(); //실제 데이터 갯수
				
				for (int i = 0; i < numData; i++) {
					Map<String, Object> tem = data.get(i);

					row = sheet.createRow(i + 1);
					for (int j = 0; j < numField; j++) {
						Object value = tem.get(field.get(j));
						if (value == null || value.equals("null")) {
							value = "";
						}
						cell = row.createCell(j);
						cell.setCellValue(String.valueOf(value));
						cell.setCellStyle(bodyStyle);
						cell.setCellType(Cell.CELL_TYPE_STRING);
					}
					
					if(i % 1000 == 0) {
						sheet.flushRows(1000);
					}
					
				}
				//실데이터 입력---------------------------

				
				// 컬럼 사이즈 조정
				for (int i = 0; i < numField; i++) {
					sheet.trackColumnForAutoSizing(i);
					sheet.autoSizeColumn(i);
					sheet.setColumnWidth(i, Math.min(255 * 256, sheet.getColumnWidth(i) + 1500));
					// 'The maximum column width for an individual cell is 255 characters.' 에러 처리

				}

			} // for a

			// WorkSheet 쓰기
	        fileOut = new FileOutputStream(outputPath); // 추가: 지정한 경로로 파일 출력 스트림 생성
	        xlsxWb.write(fileOut); // 수정: 출력 스트림에 작성
	        isSuccess = true;
	        
		} catch (Exception e) {
			e.printStackTrace();
			isSuccess = false; 
			
		} finally {
			xlsxWb.dispose();
	        fileOut.close(); // 추가: 출력 스트림 닫기

		}

			return isSuccess;
	}

}
