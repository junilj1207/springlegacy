package com.hello.spring;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.google.gson.Gson;

import com.hello.util.Excel;
import com.hello.util.FileUtil;

@Controller
@RequestMapping(value="/book")
public class BookController {

	@Autowired
	BookService BookService; 
    private Gson gson = new Gson();

	final static Logger logger = LoggerFactory.getLogger(BookController.class);
	
	 //책 입력 화면
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public String create() {
		logger.debug("create");
	    return"/book/create";
	}
	
	//책 입력 화면 로직
	@RequestMapping(value="/create", method = RequestMethod.POST)
	public ModelAndView createPost(@RequestParam Map<String,Object> map) {
		ModelAndView mav = new ModelAndView(); 
		String bookId = this.BookService.create(map);
		
		if(bookId==null){
			mav.setViewName("redirect:/book/create");
		}else{
			mav.setViewName("redirect:/book/detail?bookId="+bookId);
		}
		
		return mav;	
	}
	
	//책 상세보기
	@RequestMapping(value="/detail", method = RequestMethod.GET)
	public ModelAndView detail(@RequestParam Map<String,Object> map) {
		ModelAndView mav=new ModelAndView();	
		Map<String,Object> detailMap = this.BookService.detail(map); 
		
		mav.addObject("data",detailMap); 
		String bookId = map.get("bookId").toString(); 
		mav.addObject("bookId", bookId); 
		mav.setViewName("/book/detail");
		
		return mav; 
	}
	
	
	//책 수정 화면
	@RequestMapping(value ="/update",method = RequestMethod.GET)
	public ModelAndView update(@RequestParam Map<String,Object> map) {
		ModelAndView mav = new ModelAndView(); 
		Map<String,Object> detailMap = this.BookService.detail(map); 
		
		mav.addObject("data",detailMap);
		mav.setViewName("/book/update");		
		return mav;
	}
	
	//책 수정 로직
	@RequestMapping(value="/update",method=RequestMethod.POST)
	public ModelAndView updatePost(@RequestParam Map<String,Object> map) {
		
		ModelAndView mav = new ModelAndView(); 
		boolean isUpdateSuccess =this.BookService.edit(map); 
		if(isUpdateSuccess) {
			String bookId = map.get("bookId").toString(); 
			mav.setViewName("redirect:/book/detail?bookId="+bookId);
		}else {
			mav = this.update(map); 
		}
				
		return mav;
	}
	
	//책 삭제
	@RequestMapping(value="/delete",method=RequestMethod.POST)
	public ModelAndView deletePost(@RequestParam Map<String,Object> map) {
		ModelAndView mav = new ModelAndView(); 
		
		boolean isDeleteSuccess = this.BookService.remove(map);
		
		if(isDeleteSuccess) {
			mav.setViewName("redirect:/book/list");
		}else {
			String bookId = map.get("bookId").toString(); 
			mav.setViewName("redirect:/book/detail?bookId="+bookId);
		}
		
		return mav;
	}
	
	
	//책 리스트 
	@RequestMapping(value = "/list")
	public ModelAndView list(@RequestParam Map<String,Object> map) {
		ModelAndView mav = new ModelAndView(); 
		List<BookDto> list =this.BookService.list(map); 
		
		 Iterator<String> iter = map.keySet().iterator();
		  while(iter.hasNext()) {
	            String key = iter.next();
	            String value = (String) map.get(key);
	            
	            System.out.println(key + " : " + value);
	        }
		  
		mav.addObject("data",list); 
		mav.setViewName("/book/list");
		mav.addObject("keyword", map.get("keyword")); 
		return mav;
	}
	
	//엑셀 데이터로 엑셀 파일 만들기
	@RequestMapping(value = "/excelCreate" ,method = RequestMethod.POST)
	@ResponseBody
	public String getExcelDown(@RequestParam Map<String,Object> map) throws Exception {
		ModelAndView mav = new ModelAndView(); 
		//List<BookDto> list =this.BookService.list(map); 
		
		List<Map<String,Object>> dataList =this.BookService.list_map(map);
		List<String> fieldProgram = new ArrayList<String>(Arrays.asList(
				"price",
				"insert_date",
				"book_id",
				"title",
				"category"
				));
		
		Map<String, String> labelMapperProgram = new HashMap<String, String>();
		labelMapperProgram.put("price" , "가격");
		labelMapperProgram.put("insert_date" , "입력일");
		labelMapperProgram.put("book_id" , "번호");
		labelMapperProgram.put("title" , "제목");
		labelMapperProgram.put("category" , "카테고리");
		
		
		List<List<String>> fieldAll = new ArrayList<List<String>>();
		List<List<Map<String, Object>>> dataAll = new ArrayList<List<Map<String, Object>>>();
		List<Map<String, String>> labelMapperAll = new ArrayList<Map<String, String>>();
		List<String> sheetName = new ArrayList<String>();
		String fileName = "";

		fieldAll.add(fieldProgram);
		dataAll.add(dataList);
		labelMapperAll.add(labelMapperProgram);
		sheetName.add("개설 프로그램 정보");

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
		fileName = "ncr_stat_raw_data_" + sdf.format(new Date()) + ".xlsx";

		boolean isSuccess = false;

//		isSuccess = Excel.createExcelFileSheet("C:\\data\\excel\\"+fileName, fieldAll, dataAll, labelMapperAll, sheetName);
		isSuccess = Excel.createExcelFileSheet(File.separator +"data"+File.separator +"excel"+File.separator +fileName, fieldAll, dataAll, labelMapperAll, sheetName);
		Map<String, Object> json = new HashMap<String, Object>();
	    json.put("isSuccess", isSuccess);
		json.put("fileName", fileName);
		   
	    return gson.toJson(json); //문자열 제이슨 반환

	}
	
	//엑셀 다운로드
	@RequestMapping(value = "/download" ,method = RequestMethod.GET)
	@ResponseBody
	public String excelDownload(@RequestParam("fileName") String fileName) throws Exception{
	//public ResponseEntity<Resource>  excelDownload(@RequestParam("fileName") String fileName) throws Exception{
		 	
		
		//파일 복사
		List<File> list = FileUtil.copyFileToTemp(fileName);
	
		//해당 경로로 파일 저장
		String fullpath= File.separator+"data"+
								   	 File.separator+"excel"+
								   	 File.separator+"temp"+
								     File.separator+"temp.zip";
		
		FileUtil.createZipFile(fullpath, list);
		boolean is = true;
		Map<String, Object> json = new HashMap<String, Object>();
	    json.put("isSuccess", is);
		
	    return gson.toJson(json); //문자열 제이슨 반환

	}
	
	//링크 클릭시 파일 다운로드
	@RequestMapping(value = "/createRawDataExcelDown" ,method = RequestMethod.GET)
	public void excelDownload(HttpServletRequest request, HttpServletResponse response,@RequestParam("fileName") String fileName) throws Exception {
		
		String fullpath= File.separator+"data"+File.separator+"excel";
		
		FileUtil.download(request, response, fileName, fullpath, fileName);
	}
	
	
	//엑셀 파일 다운로드 버튼 시 이동창
	@RequestMapping(value = "/createRawDataExcelPage" ,method = RequestMethod.POST)
	public ModelAndView gotoPage(@RequestParam Map<String, Object> params) throws Exception {
		ModelAndView mav = new ModelAndView(); 
		String hello =params.get("hello").toString();

		mav.addObject("hello",hello);
		mav.setViewName("/book/downloadpage");
		return mav;
	}
	
	
	
	

}
