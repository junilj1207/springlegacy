<%@ page pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<html>  
<head>  
<title>책 목록</title>  
</head>
<script src="/resources/js/jquery-1.12.3.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {
	
	$.ajax({
		  url : "/book/excelCreate"
		, type : "post"
		, data : {
			hello:"world",
		}
	, success : function(response) {
		
		var json = JSON.parse(response);
		var _isSuccess = json.isSuccess;
		var _fileName = json.fileName;
		
        var downloadLink = $("<a>", {
            text: "파일 다운로드",
            href: "/book/createRawDataExcelDown.do?fileName="+_fileName
        });
        
        $("span#createdExcelMessage a").empty().append(downloadLink);
        
	}
	
	});

});
</script>


<body>  
<h1>엑셀 다운로드</h1>  
			<span id="createdExcelMessage">
				<h4></h4>
				<a></a>
			</span>
</body>  
</html>  