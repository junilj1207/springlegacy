<%@ page pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<html>  
<head>  
<title>책 목록</title>  
</head>
<script src="/resources/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

function excelDown(){
	try{
		

		$.ajax({
			  url : "/book/createRawDataExcelPage"
			, type : "post"
			, data : {
				hello:"world",
			}
		, success : function(response) {
			
			
		      var popupWindow = window.open("", "_blank", "width=550,height=600");
		      popupWindow.document.write(response); // Ajax로 받은 데이터를 팝업 창에 쓰기
		      popupWindow.document.close();		
		
		}
			
		});
	
	}catch(e){
		alert(e)
	}
	
}

</script>
<body>  
<h1>책 목록</h1>  
<p>  
	<form name="hello">
	</form>
	<form>  
		<input type="text" placeholder="검색" name="keyword" value="${keyword}" />  
		<input type="submit" value="검색" />  
		<input type="button" value="엑셀 다운" onClick="excelDown();"/>
		<a href="/book/download" >엑셀 다운로드</a>
		<input type="text" id="hello" value="123" />
	</form>  
</p>  
<table border="1px">  
<thead>  
	<tr>  
		<td>제목</td>  
		<td>카테고리</td>  
		<td>가격</td>  
	</tr>  
</thead>  
<tbody>  
	<c:forEach var="row" items="${data}">  
		<tr>  
		<td>  <a href="/book/detail?bookId=${row.book_id}">${row.title}</a></td>  
		<td>${row.category}</td>  
		<td><fmt:formatNumber type="number" maxFractionDigits="3" value="${row.price}" /></td>  
		</tr>  
	</c:forEach>  
</tbody>  
</table>  
<p>  
<a href="/book/create">생성</a>  
</p>  
</body>  
</html>  